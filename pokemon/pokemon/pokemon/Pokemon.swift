//
//  Pokemon.swift
//  pokemon
//
//  Created by Gabriela Cuascota on 13/6/18.
//  Copyright © 2018 Gabriela Cuascota. All rights reserved.
//

import Foundation


struct Pokemon: Decodable {
    var id:Int
    var name: String
    var weight: Int
    var height: Int
    var sprites: Sprite
    //var imagen: UIImage
}

struct Sprite: Decodable {
    var dafaultSprite: String
    
    enum CodingKeys: String, CodingKey {
        case dafaultSprite = "front_default"
    }
    
}
