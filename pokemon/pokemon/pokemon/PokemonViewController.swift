//
//  PokemonViewController.swift
//  pokemon
//
//  Created by Gabriela Cuascota on 20/6/18.
//  Copyright © 2018 Gabriela Cuascota. All rights reserved.
//

import UIKit

class PokemonViewController: UIViewController {
    
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var pesoLabel: UILabel!
    @IBOutlet weak var alturaLabel: UILabel!
    @IBOutlet weak var ImagenView: UIImageView!
    
    var pokemon:Pokemon!
    var image:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nombreLabel.text = pokemon.name
        pesoLabel.text = "\(pokemon.weight)"
        alturaLabel.text = "\(pokemon.height)"
        idLabel.text = "Pokemon Detail"
        
        let network = Network()
        network.getImage(pokemon.id) { (image) in
            self.ImagenView.image = image
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /* func fillData(){
     
     idLabel.text = "\(pokemon.id)"
     nombreLabel.text = "\(pokemon.name ?? "")"
     pesoLabel.text = "\(pokemon.weight)"
     alturaLabel.text = "\(pokemon.height)"
     
     if pokemon.imagen == nil {
     
     let bm = Network()
     
     bm.getImage((pokemon?.id)!, completionHandler: { (imageR) in
     
     DispatchQueue.main.async {
     self.ImagenView.image = imageR
     }
     
     })
     
     } else {
     
     ImagenView.image = pokemon.imagen
     
     }∫
     
     }
     */
    
    
}
