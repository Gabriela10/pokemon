//
//  ViewController.swift
//  pokemon
//
//  Created by Gabriela Cuascota on 13/6/18.
//  Copyright © 2018 Gabriela Cuascota. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

   
    var pokemon:[Pokemon] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let network = Network()
        network.getAllPokemon { (pokemonArray) in
            self.pokemon = pokemonArray
            self.tableView.reloadData()
        }
    }

     @IBOutlet weak var tableView: UITableView!

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = pokemon[indexPath.row].name
        //cell.fillData()
        return cell
    }

}

